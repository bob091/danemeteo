from django.db import models

class Meteo_data(models.Model):
    id_stacji = models.IntegerField()
    stacja = models.CharField(max_length=50)
    data_pomiaru = models.CharField(max_length=50)
    godzina_pomiaru = models.CharField(max_length=50)
    temperatura = models.CharField(max_length=50)
    predkosc_wiatru = models.CharField(max_length=50)
    kierunek_wiatru = models.CharField(max_length=50)
    wilgotnosc_wzgledna = models.CharField(max_length=50)
    suma_opadu = models.CharField(max_length=50)
    cisnienie = models.CharField(max_length=50)