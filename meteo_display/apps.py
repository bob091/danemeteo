from django.apps import AppConfig


class MeteoDisplayConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'meteo_display'
