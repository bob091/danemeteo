from rest_framework import serializers
from .models import Meteo_data
class TodoSerializer(serializers.BaseSerializer):
    class Meta:
        model = Meteo_data
        fields = ["id_stacji", "stacja", "data_pomiaru", "godzina_pomiaru", "temperatura", "predkosc_wiatru",
                  "kierunek_wiatru", "wilgotnosc_wzgledna", "suma_opadu", "cisnienie"]

        def to_representation(self, instance):
            return {
                'id_stacji': instance.id_stacji,
            }