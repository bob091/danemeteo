from django.shortcuts import render
from rest_framework.decorators import api_view
from .forms import ChoiceForms
import requests

import matplotlib.pyplot as plt
import pandas as pand
import csv



@api_view(['GET', 'POST'])
def index(request):
    """Strona głowna aplikacji"""
    # Obiekt formularza wybory miasta
    city_choce_form = ChoiceForms()

    #Jeśli  metoda GET(Wybrano miasto) - tworzymy link do api z dostepem do danego miasta
    if request.method == "GET":
        city = request.GET.get('wybierz_miasto', False)
        api_url = f'https://danepubliczne.imgw.pl/api/data/synop/id/{city}'
    else:
        # Jesle brak wybranego miasta domyslne miasto Białystok
        api_url = 'https://danepubliczne.imgw.pl/api/data/synop/id/12295'

    # Utowrzenie obiektu requests umożliwającego dostęp dod danych z REST api - zwraca dane dla danego miasta
    resp = requests.get(api_url)
    # Konwersja danych na format json
    data = resp.json()

    #Popbranie danych z pliku csv
    data_from_csv = open_and_read_file_csv()

    #pusty słownik na dane do przetworzenia
    data_for_plot = {
        'godzina_pomiaru': [],
        'temperatura': []
    }

    #umieszczebie danych z słowniku oraz konwersja na wartości liczbowe
    for d in data_from_csv:
        data_for_plot.get('godzina_pomiaru').append(pand.to_datetime(d['Godzina pomiaru']))
        data_for_plot.get('temperatura').append(pand.to_numeric(d['Temperatura']))

    # tworzymy obiekt pandas
    data_frame = pand.DataFrame(data_for_plot, columns=['godzina_pomiaru', 'temperatura'])
    # data_frame to obekt pandas
    data_frame.plot(x='godzina_pomiaru', y='temperatura', kind='bar')  # bar- wykres słóbkowy
    plt.savefig("meteo_display\static\img\plot_temp\my_plot.png")# zapis wykresy do pliku png który
    return render(request, 'meteo_display/index.html', {'city_choice_form': city_choce_form, 'data': data})

def open_and_read_file_csv() :
    """Metoda zwracająca zawartość wskazanego pliku csv"""
    file = open("meteo_display\\static\\file\\temperatury.csv", "r")
    dat = list(csv.DictReader(file, delimiter=";"))
    file.close()
    return dat

